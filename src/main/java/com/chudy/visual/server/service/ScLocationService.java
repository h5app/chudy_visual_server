package com.chudy.visual.server.service;

import com.chudy.visual.server.model.ScLocation;
import com.jfinal.plugin.activerecord.Page;

public class ScLocationService {
	/**

	 */
	private ScLocation dao = new ScLocation().dao();

	public Page<ScLocation> paginate(int pageNumber, int pageSize) {
		return dao.paginate(pageNumber, pageSize, "select *", "from Sc_Location order by id asc");
	}
	
	public Page<ScLocation> queryByPid(int pageNumber, int pageSize,String pid) {
		return dao.paginate(pageNumber, pageSize, "select *", "from Sc_Location where pid=?",pid);
	}
	
	public Page<ScLocation> queryNotTop(int pageNumber, int pageSize) {
		return dao.paginate(pageNumber, pageSize, "select *", "from Sc_Location where pid<>0");
	}

	public ScLocation findById(int id) {
		return dao.findById(id);
	}

	public void deleteById(int id) {
		dao.deleteById(id);
	}
}
