/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Net {

    private String iface;
    private String ifaceName;
    private String ip4;
    private String ip6;
    private String mac;
    private boolean internal;
    private String operstate;
    private String type;
    private String duplex;
    private String mtu;
    private int speed;
    private int carrierChanges;
    public void setIface(String iface) {
         this.iface = iface;
     }
     public String getIface() {
         return iface;
     }

    public void setIfaceName(String ifaceName) {
         this.ifaceName = ifaceName;
     }
     public String getIfaceName() {
         return ifaceName;
     }

    public void setIp4(String ip4) {
         this.ip4 = ip4;
     }
     public String getIp4() {
         return ip4;
     }

    public void setIp6(String ip6) {
         this.ip6 = ip6;
     }
     public String getIp6() {
         return ip6;
     }

    public void setMac(String mac) {
         this.mac = mac;
     }
     public String getMac() {
         return mac;
     }

    public void setInternal(boolean internal) {
         this.internal = internal;
     }
     public boolean getInternal() {
         return internal;
     }

    public void setOperstate(String operstate) {
         this.operstate = operstate;
     }
     public String getOperstate() {
         return operstate;
     }

    public void setType(String type) {
         this.type = type;
     }
     public String getType() {
         return type;
     }

    public void setDuplex(String duplex) {
         this.duplex = duplex;
     }
     public String getDuplex() {
         return duplex;
     }

    public void setMtu(String mtu) {
         this.mtu = mtu;
     }
     public String getMtu() {
         return mtu;
     }

    public void setSpeed(int speed) {
         this.speed = speed;
     }
     public int getSpeed() {
         return speed;
     }

    public void setCarrierChanges(int carrierChanges) {
         this.carrierChanges = carrierChanges;
     }
     public int getCarrierChanges() {
         return carrierChanges;
     }

}