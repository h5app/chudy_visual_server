/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.adrirodclient;

/**
 * Auto-generated: 2019-07-18 18:7:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class _customDevice {

    private String deviceid;
    private String platform;
    private Displaysize displaysize;
    public void setDeviceid(String deviceid) {
         this.deviceid = deviceid;
     }
     public String getDeviceid() {
         return deviceid;
     }

    public void setPlatform(String platform) {
         this.platform = platform;
     }
     public String getPlatform() {
         return platform;
     }

    public void setDisplaysize(Displaysize displaysize) {
         this.displaysize = displaysize;
     }
     public Displaysize getDisplaysize() {
         return displaysize;
     }

}