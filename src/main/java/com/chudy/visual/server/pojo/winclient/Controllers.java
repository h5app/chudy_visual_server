/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;
import java.util.Date;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Controllers {

    private String model;
    private String vendor;
    private String bus;
    private int vram;
    private boolean vramDynamic;
    public void setModel(String model) {
         this.model = model;
     }
     public String getModel() {
         return model;
     }

    public void setVendor(String vendor) {
         this.vendor = vendor;
     }
     public String getVendor() {
         return vendor;
     }

    public void setBus(String bus) {
         this.bus = bus;
     }
     public String getBus() {
         return bus;
     }

    public void setVram(int vram) {
         this.vram = vram;
     }
     public int getVram() {
         return vram;
     }

    public void setVramDynamic(boolean vramDynamic) {
         this.vramDynamic = vramDynamic;
     }
     public boolean getVramDynamic() {
         return vramDynamic;
     }

}