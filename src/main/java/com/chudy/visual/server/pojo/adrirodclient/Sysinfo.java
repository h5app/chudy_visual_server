/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.adrirodclient;

/**
 * Auto-generated: 2019-07-18 18:7:31
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Sysinfo {

    private _customDevice _customDevice;
    private Os os;
    private Device device;
    private Networkinfo networkinfo;
    public void set_customDevice(_customDevice _customDevice) {
         this._customDevice = _customDevice;
     }
     public _customDevice get_customDevice() {
         return _customDevice;
     }

    public void setOs(Os os) {
         this.os = os;
     }
     public Os getOs() {
         return os;
     }

    public void setDevice(Device device) {
         this.device = device;
     }
     public Device getDevice() {
         return device;
     }

    public void setNetworkinfo(Networkinfo networkinfo) {
         this.networkinfo = networkinfo;
     }
     public Networkinfo getNetworkinfo() {
         return networkinfo;
     }

}