/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Versions {

    private String kernel;
    private String openssl;
    private String systemOpenssl;
    private String systemOpensslLib;
    private String node;
    private String v8;
    private String npm;
    private String yarn;
    private String pm2;
    private String gulp;
    private String grunt;
    private String git;
    private String tsc;
    private String mysql;
    private String redis;
    private String mongodb;
    private String apache;
    private String nginx;
    private String php;
    private String docker;
    private String postfix;
    private String postgresql;
    private String perl;
    private String python;
    private String python3;
    private String pip;
    private String pip3;
    private String java;
    private String gcc;
    public void setKernel(String kernel) {
         this.kernel = kernel;
     }
     public String getKernel() {
         return kernel;
     }

    public void setOpenssl(String openssl) {
         this.openssl = openssl;
     }
     public String getOpenssl() {
         return openssl;
     }

    public void setSystemOpenssl(String systemOpenssl) {
         this.systemOpenssl = systemOpenssl;
     }
     public String getSystemOpenssl() {
         return systemOpenssl;
     }

    public void setSystemOpensslLib(String systemOpensslLib) {
         this.systemOpensslLib = systemOpensslLib;
     }
     public String getSystemOpensslLib() {
         return systemOpensslLib;
     }

    public void setNode(String node) {
         this.node = node;
     }
     public String getNode() {
         return node;
     }

    public void setV8(String v8) {
         this.v8 = v8;
     }
     public String getV8() {
         return v8;
     }

    public void setNpm(String npm) {
         this.npm = npm;
     }
     public String getNpm() {
         return npm;
     }

    public void setYarn(String yarn) {
         this.yarn = yarn;
     }
     public String getYarn() {
         return yarn;
     }

    public void setPm2(String pm2) {
         this.pm2 = pm2;
     }
     public String getPm2() {
         return pm2;
     }

    public void setGulp(String gulp) {
         this.gulp = gulp;
     }
     public String getGulp() {
         return gulp;
     }

    public void setGrunt(String grunt) {
         this.grunt = grunt;
     }
     public String getGrunt() {
         return grunt;
     }

    public void setGit(String git) {
         this.git = git;
     }
     public String getGit() {
         return git;
     }

    public void setTsc(String tsc) {
         this.tsc = tsc;
     }
     public String getTsc() {
         return tsc;
     }

    public void setMysql(String mysql) {
         this.mysql = mysql;
     }
     public String getMysql() {
         return mysql;
     }

    public void setRedis(String redis) {
         this.redis = redis;
     }
     public String getRedis() {
         return redis;
     }

    public void setMongodb(String mongodb) {
         this.mongodb = mongodb;
     }
     public String getMongodb() {
         return mongodb;
     }

    public void setApache(String apache) {
         this.apache = apache;
     }
     public String getApache() {
         return apache;
     }

    public void setNginx(String nginx) {
         this.nginx = nginx;
     }
     public String getNginx() {
         return nginx;
     }

    public void setPhp(String php) {
         this.php = php;
     }
     public String getPhp() {
         return php;
     }

    public void setDocker(String docker) {
         this.docker = docker;
     }
     public String getDocker() {
         return docker;
     }

    public void setPostfix(String postfix) {
         this.postfix = postfix;
     }
     public String getPostfix() {
         return postfix;
     }

    public void setPostgresql(String postgresql) {
         this.postgresql = postgresql;
     }
     public String getPostgresql() {
         return postgresql;
     }

    public void setPerl(String perl) {
         this.perl = perl;
     }
     public String getPerl() {
         return perl;
     }

    public void setPython(String python) {
         this.python = python;
     }
     public String getPython() {
         return python;
     }

    public void setPython3(String python3) {
         this.python3 = python3;
     }
     public String getPython3() {
         return python3;
     }

    public void setPip(String pip) {
         this.pip = pip;
     }
     public String getPip() {
         return pip;
     }

    public void setPip3(String pip3) {
         this.pip3 = pip3;
     }
     public String getPip3() {
         return pip3;
     }

    public void setJava(String java) {
         this.java = java;
     }
     public String getJava() {
         return java;
     }

    public void setGcc(String gcc) {
         this.gcc = gcc;
     }
     public String getGcc() {
         return gcc;
     }

}