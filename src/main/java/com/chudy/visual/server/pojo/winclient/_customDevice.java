/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class _customDevice {

    private String deviceid;
    private Displaysize displaysize;
    private String platform; //win32 / adrirod
    public void setDeviceid(String deviceid) {
         this.deviceid = deviceid;
     }
     public String getDeviceid() {
         return deviceid;
     }

    public void setDisplaysize(Displaysize displaysize) {
         this.displaysize = displaysize;
     }
     public Displaysize getDisplaysize() {
         return displaysize;
     }
	public String getPlatform() {
		return platform;
	}
	public void setPlatform(String platform) {
		this.platform = platform;
	}
     
     

}