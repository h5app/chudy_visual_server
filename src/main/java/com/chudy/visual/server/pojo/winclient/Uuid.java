/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Uuid {

    private String os;
    public void setOs(String os) {
         this.os = os;
     }
     public String getOs() {
         return os;
     }

}