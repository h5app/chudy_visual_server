/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.push;

import java.util.List;

/**
 * Auto-generated: 2019-08-01 11:44:37
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class PushList {

	private String pushtype;
	private List<Contentlist> contentlist;

	public void setPushtype(String pushtype) {
		this.pushtype = pushtype;
	}

	public String getPushtype() {
		return pushtype;
	}

	public void setContentlist(List<Contentlist> contentlist) {
		this.contentlist = contentlist;
	}

	public List<Contentlist> getContentlist() {
		return contentlist;
	}

}