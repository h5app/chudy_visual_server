/**
  * Copyright 2019 bejson.com 
  */
package com.chudy.visual.server.pojo.winclient;
import java.util.List;

/**
 * Auto-generated: 2019-05-15 11:18:14
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Graphics {

    private List<Controllers> controllers;
    private List<Displays> displays;
    public void setControllers(List<Controllers> controllers) {
         this.controllers = controllers;
     }
     public List<Controllers> getControllers() {
         return controllers;
     }

    public void setDisplays(List<Displays> displays) {
         this.displays = displays;
     }
     public List<Displays> getDisplays() {
         return displays;
     }

}