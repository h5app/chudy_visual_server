package com.chudy.visual.server.index;

import java.util.HashMap;
import java.util.Map;

import com.chudy.visual.server.model.ScContentDelivery;
import com.chudy.visual.server.push.VisualPushUtils;
import com.chudy.visual.server.service.ScContentService;
import com.chudy.visual.server.service.ScDeviceService;
import com.chudy.visual.server.service.ScLocationService;
import com.jfinal.aop.Inject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Ret;

/**
 * IndexController
 */
public class PushController extends Controller {
	@Inject
	ScContentService scContentService;
	@Inject
	ScDeviceService scDeviceService;

	public void index() {
		String text = "Hello.感谢使用触达云屏.";
		render("index.html");
	}

	public void deviceList() {
		renderJson(scDeviceService.paginate(1,10));
	}
	public void content() {
		String contentData = HttpKit.readData(getRequest());
		String deviceid = getPara("deviceid");
		ScContentDelivery contentDelivery = new ScContentDelivery();
		contentDelivery.setContentData(contentData);
		contentDelivery.setDeviceid(deviceid);
		scContentService.pushContent(contentDelivery);
		Ret ret = new Ret();
		Ret data = new Ret();
		data.put("deviceid", deviceid);
		ret.put("code", "0");
		ret.put("data", data);
		renderJson(ret);
	}
}
