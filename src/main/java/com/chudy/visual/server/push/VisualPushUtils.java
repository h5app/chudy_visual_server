package com.chudy.visual.server.push;

import java.util.HashMap;
import java.util.List;

import org.tio.core.ChannelContext;
import org.tio.core.Tio;
import org.tio.websocket.common.WsResponse;

import com.chudy.visual.server.plugins.TioConst;
import com.chudy.visual.server.websocket.Const;
import com.chudy.visual.server.websocket.VisualServerConfig;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.StrKit;

public class VisualPushUtils {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static String pushAll(String msgjson) {
		WsResponse wsResponse = WsResponse.fromText(msgjson, VisualServerConfig.CHARSET);
		Tio.sendToAll(TioConst.appStarter.getServerGroupContext(), wsResponse);
		return "";
	}

	public static String push(String deviceid, String msgjson) {
		WsResponse wsResponse = WsResponse.fromText(msgjson, VisualServerConfig.CHARSET);
		ChannelContext clientContext = Tio.getChannelContextByBsId(TioConst.appStarter.getServerGroupContext(),
				deviceid);
		Tio.bSendToUser(TioConst.appStarter.getServerGroupContext(), deviceid, wsResponse);
		return "";
	}

	public static String pushGroup(String groupid, String msgjson) {
		WsResponse wsResponse = WsResponse.fromText(msgjson, VisualServerConfig.CHARSET);
		Tio.sendToGroup(TioConst.appStarter.getServerGroupContext(), groupid, wsResponse);
		return "";
	}
	
	public static String pushDeviceUpdate(String groupid, String deviceid) {
		HashMap<String, Object> msg = new HashMap<String, Object>();
		HashMap<String, String> data = new HashMap<String, String>();
		msg.put("pushtype", "event");
		data.put("key", "checkUpdate");
		msg.put("data", data);
		WsResponse wsResponse = WsResponse.fromText(JsonKit.toJson(msg), VisualServerConfig.CHARSET);
		if(StrKit.notBlank(deviceid)){
			Tio.bSendToUser(TioConst.appStarter.getServerGroupContext(), deviceid, wsResponse);
		}else{
			Tio.sendToGroup(TioConst.appStarter.getServerGroupContext(), Const.GROUP_DEVICE_ID, wsResponse);
		}
		return "";
	}

	public static List getClientOnlineList(String group) {
		return null;
	}

}
