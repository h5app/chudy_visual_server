package com.chudy.visual.server.utils.img;

/**
 * @Description 自定义颜色类，不是很准确的，但是提取纯色，例如黑白完全没问题
 * @author 姚旭民
 * @date 2019/7/12 18:11
 */
public enum MyColor {// 自己划定的几个颜色的颜色分类，会存在识别不出来的颜色
	COLOR_BLACK(0, 66, 0, 66, 0, 66, "黑色", 0), COLOR_GRAY(67, 190, 67, 190, 67, 190, "灰色", 0),
	COLOR_WHITE(230, 255, 230, 255, 230, 255, "白色", 0), COLOR_RED(70, 255, 0, 200, 0, 200, "红色", 0),
	COLOR_YELLOW(220, 255, 140, 230, 0, 160, "黄色", 0), COLOR_ORANGE(200, 255, 85, 110, 0, 61, "橙色", 0),
	COLOR_GREEN(0, 127, 60, 255, 0, 205, "绿色", 0), COLOR_CHING(0, 160, 120, 255, 84, 255, "青色", 0),
	COLOR_BLUE(0, 25, 25, 60, 150, 255, "蓝色", 0), COLOR_PURPLE(80, 218, 32, 112, 64, 250, "紫色", 0);

	private MyColor(int rMin, int rMax, int gMin, int gMax, int bMin, int bMax, String name, double parcent) {
		this.rMin = rMin;
		this.rMax = rMax;
		this.gMin = gMin;
		this.gMax = gMax;
		this.bMin = bMin;
		this.bMax = bMax;
		this.name = name;
		this.parcent = parcent;
	}

	private int rMin, rMax, gMin, gMax, bMin, bMax;
	private double parcent;
	private String name;

	public int getrMin() {
		return rMin;
	}

	public int getrMax() {
		return rMax;
	}

	public int getgMin() {
		return gMin;
	}

	public int getgMax() {
		return gMax;
	}

	public int getbMin() {
		return bMin;
	}

	public int getbMax() {
		return bMax;
	}

	public String getName() {
		return name;
	}

	public double getParcent() {
		return parcent;
	}

	public void setParcent(double parcent) {
		this.parcent = parcent;
	}

	/**
	 * @param r 红色色值
	 * @param g 绿色色值
	 * @param b 蓝色色值
	 * @Description 通过三原色色值查找颜色
	 * @author 姚旭民
	 * @date 2019/7/9 17:05
	 */
	static int findCount = 0;

	public static MyColor find(double r, double g, double b) {// 如果可以的话，不要改这里的顺序，因为改了结果会不一样的
		if (r <= COLOR_BLACK.rMax && r >= COLOR_BLACK.rMin && g <= COLOR_BLACK.gMax && g >= COLOR_BLACK.gMin
				&& b <= COLOR_BLACK.bMax && b >= COLOR_BLACK.bMin) {
			return COLOR_BLACK;// 黑色
		} else if (r <= COLOR_WHITE.rMax && r >= COLOR_WHITE.rMin && g <= COLOR_WHITE.gMax && g >= COLOR_WHITE.gMin
				&& b <= COLOR_WHITE.bMax && b >= COLOR_WHITE.bMin) {
			return COLOR_WHITE;// 白色
		} else if (r <= COLOR_GRAY.rMax && r >= COLOR_GRAY.rMin && g <= COLOR_GRAY.gMax && g >= COLOR_GRAY.gMin
				&& b <= COLOR_GRAY.bMax && b >= COLOR_GRAY.bMin) {
			return COLOR_GRAY;// 灰色
		} else if (r <= COLOR_RED.rMax && r >= COLOR_RED.rMin && g <= COLOR_RED.gMax && g >= COLOR_RED.gMin
				&& b <= COLOR_RED.bMax && b >= COLOR_RED.bMin) {
			return COLOR_RED;// 红色
		} else if (r <= COLOR_BLUE.rMax && r >= COLOR_BLUE.rMin && g <= COLOR_BLUE.gMax && g >= COLOR_BLUE.gMin
				&& b <= COLOR_BLUE.bMax && b >= COLOR_BLUE.bMin) {
			return COLOR_BLUE;// 蓝色
		} else if (r <= COLOR_CHING.rMax && r >= COLOR_CHING.rMin && g <= COLOR_CHING.gMax && g >= COLOR_CHING.gMin
				&& b <= COLOR_CHING.bMax && b >= COLOR_CHING.bMin) {
			return COLOR_CHING;// 青色
		} else if (r <= COLOR_GREEN.rMax && r >= COLOR_GREEN.rMin && g <= COLOR_GREEN.gMax && g >= COLOR_GREEN.gMin
				&& b <= COLOR_GREEN.bMax && b >= COLOR_GREEN.bMin) {
			return COLOR_GREEN;// 绿色
		} else if (r <= COLOR_ORANGE.rMax && r >= COLOR_ORANGE.rMin && g <= COLOR_ORANGE.gMax && g >= COLOR_ORANGE.gMin
				&& b <= COLOR_ORANGE.bMax && b >= COLOR_ORANGE.bMin) {
			return COLOR_ORANGE;// 橙色
		} else if (r <= COLOR_YELLOW.rMax && r >= COLOR_YELLOW.rMin && g <= COLOR_YELLOW.gMax && g >= COLOR_YELLOW.gMin
				&& b <= COLOR_YELLOW.bMax && b >= COLOR_YELLOW.bMin) {
			return COLOR_YELLOW;// 黄色
		} else if (r <= COLOR_PURPLE.rMax && r >= COLOR_PURPLE.rMin && g <= COLOR_PURPLE.gMax && g >= COLOR_PURPLE.gMin
				&& b <= COLOR_PURPLE.bMax && b >= COLOR_PURPLE.bMin) {
			return COLOR_PURPLE;// 紫色
		}
		if (findCount < 20) {
			//Log.i(TAG, "find| r : " + r + ",g : " + g + ",b : " + b);
			findCount++;
		}
		return null;
	}
}
