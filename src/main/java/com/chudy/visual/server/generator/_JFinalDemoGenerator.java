package com.chudy.visual.server.generator;

import java.util.ArrayList;

import javax.sql.DataSource;

import com.chudy.visual.server.run.MainConfig;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.druid.DruidPlugin;

/**
 * �?demo 仅表达最为粗浅的 jfinal 用法，更为有价�?的实用的企业级用�?
 * 详见 JFinal 俱乐�? http://jfinal.com/club
 * 
 * 在数据库表有任何变动时，运行�?�� main 方法，极速响应变化进行代码重�?
 */
public class _JFinalDemoGenerator {
	
	public static DataSource getDataSource() {
		//PropKit.use("config.properties");
		DruidPlugin druidPlugin = MainConfig.createDruidPlugin();
		druidPlugin.start();
		return druidPlugin.getDataSource();
	}
	
	public static void main(String[] args) {
		// base model �?��用的包名
		String baseModelPackageName = "com.chudy.visual.server.model.base";
		// base model 文件保存路径 \src\main\java
		String baseModelOutputDir = PathKit.getWebRootPath() + "/src/main/java/com/chudy/visual/server/model/base";
		
		// model �?��用的包名 (MappingKit 默认使用的包�?
		String modelPackageName = "com.chudy.visual.server.model";
		// model 文件保存路径 (MappingKit �?DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir + "/..";
		
		
		// 创建生成�?
		Generator generator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
//		MetaBuilderExt metaBuilder = new MetaBuilderExt(getDataSource(),new String[]{"SC_","sc_"}); 
		ArrayList<String> tables = new ArrayList<String>();
		//tables.add("sc_client_version");//客户端版本
		tables.add("sc_device");//设备
		tables.add("sc_content");//内容
		tables.add("sc_content_delivery");//内容发布
		tables.add("sc_device_dynamic");//设备动态信息
		tables.add("sc_location");//位置基础数据
		//tables.add("sc_playlist");//轮播列表
		//tables.add("sc_playlist_item");//轮播列表明细
		//tables.add("sc_screen");
		
		
		//String[] tables = new String[]();
		MetaBuilderExt metaBuilder = new MetaBuilderExt(getDataSource(),tables.toArray(new String[tables.size()])); 
		//MetaBuilderExt metaBuilder = new MetaBuilderExt(getDataSource());
		generator.setMetaBuilder(metaBuilder);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(false);
		// 添加不需要生成的表名
		generator.addExcludedTable("adv");
		// 设置是否�?Model 中生�?dao 对象
		generator.setGenerateDaoInModel(false);
		// 设置是否生成链式 setter 方法
		generator.setGenerateChainSetter(true);
		// 设置是否生成字典文件
		generator.setGenerateDataDictionary(false);
		// 设置�?��被移除的表名前缀用于生成modelName。例如表�?"osc_user"，移除前�?"osc_"后生成的model名为 "User"而非 OscUser
		generator.setRemovedTableNamePrefixes("t_");
		//generator.setDialect(new OracleDialect());
		generator.setDialect(new MysqlDialect());
		// 生成
		generator.generate();
	}
}




