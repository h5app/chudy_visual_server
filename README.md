# 触达云屏-大屏设备内容推送服务端

#### 介绍

 触达云屏-大屏设备内容推送服务端  和 触达云屏-Windows设备大屏展示客户端  结合使用。

 实现所有大屏设备展示内容远程发布。

#### 软件架构
系统使用国产框架：jfinal（极简设计） + websocket(使用tio，简单高效)

![输入图片说明](src/main/doc/%E8%A7%A6%E8%BE%BE%E4%BA%91%E5%B1%8F-%E5%86%85%E5%AE%B9%E5%8F%91%E5%B8%83%E6%9E%B6%E6%9E%84.png)


#### 安装、使用教程

确保电脑上已经安装java1.8+的环境
1.  下载发行版，解压
2.  安装mysql数据库
3.  创建数据库 chudy_visual_server,执行初始化脚本 doc 目录下的 init20220309.sql
4.  修改配置、运行 start.bat
5.  访问 http://127.0.0.1:8078/push

![输入图片说明](src/main/doc/image.png)


#### 开发扩展


1. idea 和 eclipce 作为开发工具
1. 运行 com.chudy.visual.server.run.MainConfig ，main函数启动项目


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 需求、建议

欢迎大家反馈需求和建议。
沟通探讨QQ群：160269112